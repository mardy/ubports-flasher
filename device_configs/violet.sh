DEVICE_NAME="Xiaomi Redmi Note 7 Pro"

DEVICE_TYPE="android"

BOOTSTRAP_PARTITIONS=(
    ["splash"]="https://gitlab.com/ubports/community-ports/android9/xiaomi-redmi-note-7-pro/xiaomi-violet/-/jobs/artifacts/master/raw/out/splash.img?job=devel-flashable"
    ["recovery"]="https://gitlab.com/ubports/community-ports/android9/xiaomi-redmi-note-7-pro/xiaomi-violet/-/jobs/artifacts/master/raw/out/recovery.img?job=devel-flashable"
    ["vendor"]="https://github.com/ubuntu-touch-violet/ubuntu-touch-violet/releases/download/20210510/vendor.zip"
    ["dtbo"]="https://github.com/ubuntu-touch-violet/ubuntu-touch-violet/releases/download/20210510/dtbo.img"
    ["dsp"]="https://github.com/ubuntu-touch-violet/ubuntu-touch-violet/releases/download/20210510/dsp.img"
    ["bluetooth"]="https://github.com/ubuntu-touch-violet/ubuntu-touch-violet/releases/download/20210510/bluetooth.img"
    ["modem"]="https://github.com/ubuntu-touch-violet/ubuntu-touch-violet/releases/download/20210510/modem.img"
    ["vbmeta"]="https://github.com/ubuntu-touch-violet/ubuntu-touch-violet/releases/download/20210510/vbmeta.img"
)

FILE_CHECKSUMS=(
    ["vendor.zip"]="sha256:3aa2db9ce47698d568ca9368c5e23e1b2f19f69f58552b1b8dd63728c7e9b8cc"
    ["dtbo.img"]="sha256:dbeba15ce9345e1a43779b6509fd4dd76eb2c521610eb15203abdcecdb82261b"
    ["dsp.img"]="sha256:67267993bc3256de9ddb367a8083f66eec2e9036347a1dc2374d8e7e5958462e"
    ["bluetooth.img"]="sha256:b3d78c2c01ea17c78a49ef604cfcde3dc72dcdf7f34c9aa09ce72eded7cbc2f0"
    ["modem.img"]="sha256:e7155f8095a45f1a925bca9c81df145a2e8a90e359a85b5face3986f520ba71c"
    ["vbmeta.img"]="sha256:f734812efec31d2c10a45dbcf9aa49b9994d3ad3cb221a5938e1a44362aa64ad"
)

violet_prepare_partition_images () {
    # chain up to the base implementation
    android_prepare_partition_images

    # Get the vendor zip file, and unpack it
    local ZIP_FILE="${BOOTSTRAP_PARTITION_IMAGES["vendor"]}"
    unzip "$ZIP_FILE" -d "$TMP_DIR"
    local IMG_FILE="$TMP_DIR/vendor.img"
    BOOTSTRAP_PARTITION_IMAGES["vendor"]="$IMG_FILE"
}
