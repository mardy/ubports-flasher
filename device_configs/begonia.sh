DEVICE_NAME="Xiaomi Redmi Note 8 Pro"

DEVICE_TYPE="android"

# LCOV_EXCL_START
BOOTSTRAP_PARTITIONS=(
    ["recovery"]="https://github.com/ubuntu-touch-begonia/ubuntu-touch-begonia/releases/download/20210810/recovery.img"
    ["vendor"]="https://github.com/ubuntu-touch-begonia/ubuntu-touch-begonia/releases/download/20210810/vendor.zip"
    ["dtbo"]="https://github.com/ubuntu-touch-begonia/ubuntu-touch-begonia/releases/download/20210810/dtbo.img"
    ["vbmeta"]="https://github.com/ubuntu-touch-begonia/ubuntu-touch-begonia/releases/download/20210810/vbmeta.img"
)

BOOTSTRAP_EXTRA_DOWNLOADS=(
    "https://github.com/ubuntu-touch-begonia/ubuntu-touch-begonia/releases/download/20210810/fw_begonia_V12.0.8.0.zip"
)

# begonia "adb push" doesn't work reliably; therefore, instead of pushing the
# OS files from the system-image server, we directly flash a full image with
# fastboot; it will then be updated by the OS itself.
declare -Ag OS_PARTITIONS
OS_PARTITIONS=(
    ["system"]="https://github.com/ubuntu-touch-begonia/ubuntu-touch-begonia/releases/download/ota-19/system.zip"
    ["boot"]="https://github.com/ubuntu-touch-begonia/ubuntu-touch-begonia/releases/download/ota-19/boot.img"
)

FILE_CHECKSUMS=(
    ["recovery.img"]="sha256:9facd6ab47459f85aab4ee1f43b261aac66af710bafbe3f4f2cea330e56fe8d1"
    ["vendor.zip"]="sha256:2311be025c0a127a69dae648ffc7001e86ffbdfdd7bf02e20d5079e7c99da106"
    ["dtbo.img"]="sha256:7bcf9b85133b3e748ac72d27d0a3f58be9c1f6ac96c9c5b866b684b266dcc108"
    ["vbmeta.img"]="sha256:b3281c527ecf2a6a743b37f5b9e118f492d2038e0a82e6777d292d1c5b42601a"
    ["system.zip"]="sha256:fff166da969228b6f9359a7c064710933397be6ad62efaae9162600027137341"
    ["boot.img"]="sha256:fa92b082823be3b8527b81e720dc59835d13c8898fe513009f44285821e98d4f"
)

# LCOV_EXCL_STOP

# begonia_unpack_partition_image IMAGE
begonia_unpack_partition_image() {
    local IMAGE="$1"
    local ZIP_FILE="${BOOTSTRAP_PARTITION_IMAGES["$IMAGE"]}"
    unzip "$ZIP_FILE" -d "$TMP_DIR"
    local IMG_FILE="$TMP_DIR/$IMAGE.img"
    BOOTSTRAP_PARTITION_IMAGES["$IMAGE"]="$IMG_FILE"
}

begonia_prepare_partition_images() {
    # chain up to the base implementation
    android_prepare_partition_images
    begonia_unpack_partition_image vendor
    local FIRMWARE_URL="${BOOTSTRAP_EXTRA_DOWNLOADS[0]}"
    local FIRMWARE_ZIP_FILE="$(get_downloaded_file "$FIRMWARE_URL")"
    unzip "$FIRMWARE_ZIP_FILE" -d "$TMP_DIR"
    # LCOV_EXCL_START
    local -A FIRMWARE_PARTITIONS=(
        ["sspm_1"]="sspm.img"
        ["lk"]="lk.img"
        ["gz1"]="gz.img"
        ["scp1"]="scp.img"
        ["tee1"]="tee.img"
        ["sspm_2"]="sspm.img"
        ["lk2"]="lk.img"
        ["gz2"]="gz.img"
        ["scp2"]="scp.img"
        ["tee2"]="tee.img"
        ["preloader"]="preloader_ufs.img"
    )
    # LCOV_EXCL_STOP
    for p in audio_dsp cam_vpu1 cam_vpu2 cam_vpu3 md1img spmfw
    do
        FIRMWARE_PARTITIONS["$p"]="${p}.img"
    done
    for p in "${!FIRMWARE_PARTITIONS[@]}"
    do
        local IMG_FILE="$TMP_DIR/${FIRMWARE_PARTITIONS[$p]}"
        BOOTSTRAP_PARTITION_IMAGES["$p"]="$IMG_FILE"
    done
}

# Since we do not use the system-image server, we don't need this step
begonia_init_channel() {
    debug "Skipping init_channel step for begonia"
}

begonia_download_os() {
    local DEST_DIR="$CACHE_DIR/$DEVICE/Ubuntu Touch"
    mkdir -p "$DEST_DIR"
    download_files "$DEST_DIR" "${OS_PARTITIONS[@]}"
}

begonia_unpack_os_partition_image() {
    local PARTITION="$1"
    local PARTITION_URL="${OS_PARTITIONS["$PARTITION"]}"
    local PARTITION_ZIP_FILE="$(get_downloaded_file "$PARTITION_URL")"
    unzip "$PARTITION_ZIP_FILE" -d "$TMP_DIR"
}

begonia_flash_channel() {
    test "$WIPE_USERDATA" = "yes" &&
        action format userdata ext4
    action ensure_boot "bootloader"
    if [ "$BOOTSTRAP" = "yes" ]; then
        action download_firmware
        action flash_firmware
    fi

    action download_os
    begonia_unpack_os_partition_image system
    fastboot flash system "$TMP_DIR/system.img"
    fastboot flash boot "$CACHE_DIR/$DEVICE/Ubuntu Touch/boot.img"
    fastboot reboot
}
