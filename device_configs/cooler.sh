DEVICE_NAME="Bq Aquaris M10 HD"
DEVICE_ALIASES="Aquaris_M10HD BQ_AQUARIS_M10"

DEVICE_TYPE="android"

BOOTSTRAP_PARTITIONS=(
    ["recovery"]="http://cdimage.ubports.com/devices/recovery-cooler.img"
)

FILE_CHECKSUMS=(
    ["recovery-cooler.img"]="sha256:07072e9e802b5e66546bded3fa0ad2a093fd2d14bf87f6b7138b148038040579"
)
