DEVICE_NAME="Xiaomi Redmi Note 9 Pro/Pro Max/9S - Poco M2 Pro"
DEVICE_ALIASES="joyeuse curtana excalibur gram"

DEVICE_TYPE="android"

BOOTSTRAP_PARTITIONS=(
    ["recovery"]="https://github.com/ubuntu-touch-miatoll/ubuntu-touch-miatoll/releases/download/stable-installer-fix/recovery.img"
    ["dtbo"]="https://github.com/ubuntu-touch-miatoll/ubuntu-touch-miatoll/releases/download/stable-installer-fix/dtbo.img"
)

FILE_CHECKSUMS=(
    ["dtbo.img"]="sha256:d408a9ecd7d2c6098cc06ec7dab03f69a532741f3626e6188d57b7557f421eb8"
    ["recovery.img"]="sha256:461259015c4bdd0e8fb36c7412454f9c8c9e8ae6b1413e79598bd1c6ec0095ea"
)
