DEVICE_NAME="Bq Aquaris M10 FHD"
DEVICE_ALIASES="Aquaris_M10FHD"

DEVICE_TYPE="android"

BOOTSTRAP_PARTITIONS=(
    ["recovery"]="http://cdimage.ubports.com/devices/recovery-frieza.img"
)

FILE_CHECKSUMS=(
    ["recovery-frieza.img"]="sha256:374f184e2675f53651cf4790ee685ee363f59f2be98ea99a6d9d948bbc168c8b"
)
