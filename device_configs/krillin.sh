DEVICE_NAME="Bq Aquaris E4.5"
DEVICE_ALIASES="Aquaris_E45"

DEVICE_TYPE="android"

# LCOV_EXCL_START
BOOTSTRAP_PARTITIONS=(
    ["recovery"]="http://cdimage.ubports.com/devices/recovery-krillin.img"
)

FILE_CHECKSUMS=(
    ["recovery-krillin.img"]="sha256:d2b57e9f886e55c263bcff9ba7553d2cb0e00d74411490bfb62f252cda3a86e5"
)
# LCOV_EXCL_STOP
