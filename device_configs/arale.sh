DEVICE_NAME="Meizu MX4"
DEVICE_ALIASES="mx4"

DEVICE_TYPE="android"

BOOTSTRAP_PARTITIONS=(
    ["recovery"]="http://cdimage.ubports.com/devices/recovery-arale.img"
)

FILE_CHECKSUMS=(
    ["recovery-arale.img"]="sha256:27160d1ce2d55bd940b38ebf643018b33e0516795dff179942129943fabdc3d8"
)
