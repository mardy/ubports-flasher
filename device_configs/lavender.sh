DEVICE_NAME="Xiaomi Redmi Note 7"

DEVICE_TYPE="android"

BOOTSTRAP_PARTITIONS=(
    ["splash"]="https://cdimage.ubports.com/devices/lavender/splash.img"
    ["recovery"]="https://cdimage.ubports.com/devices/lavender/recovery.img"
    ["vendor"]="vendor.img"
    ["dtbo"]="https://cdimage.ubports.com/devices/lavender/dtbo.img"
    ["vbmeta"]="https://cdimage.ubports.com/devices/lavender/vbmeta.img"
)

FILE_CHECKSUMS=(
    ["vendor.img"]="sha256:7b26a7ddc779079c04dbddd983dc1e6108a7711cac26cff06bc9f4309a4edbc7"
    ["dtbo.img"]="sha256:84ce5cc1549a87da0ac9e6545251f1317de5ce2322d54c384c366c9bd2d8efc6"
    ["recovery.img"]="sha256:990a377a0a87809a9ceb4f85c693e7c7c190224ff98e18a8a0e21966688bda0b"
    ["vbmeta.img"]="sha256:65f5493e01ede7e538a14df063013a921ecd122680c7d3b3598d46fc7a916517"
    ["splash.img"]="sha256:fd1ac18cb18a16b14d6d40da6abf0538724d3e83f4d5e60615d5b7038dce7c50"
)

MANUAL_DOWNLOADS=(
    ["vendor.img"]="https://www.androidfilehost.com/?fid=10763459528675594236"
)
