DEVICE_NAME="Nexus 6P"

DEVICE_TYPE="android"

BOOTSTRAP_PARTITIONS=(
    ["recovery"]="https://cdimage.ubports.com/devices/angler/halium-unlocked-recovery_angler.img"
)

FILE_CHECKSUMS=(
    ["halium-unlocked-recovery_angler.img"]="sha256:9fb583720bd6a1ecbb1d6ce9c0714b49f3f82f2c00c51a0f6260e247d4bad288"
)

angler_flash_channel() {
    confirm_user "Please ensure that your device is running stock Android 7.1.2 —\nCritical vendor files are otherwise missing!"
    confirm_user "Please install TWRP recovery and format all partitions due to the encryption applied by the stock ROM"
    android_flash_channel
}
