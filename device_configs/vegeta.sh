DEVICE_NAME="Bq Aquaris E5"
DEVICE_ALIASES="Aquaris_E5"

DEVICE_TYPE="android"

BOOTSTRAP_PARTITIONS=(
    ["recovery"]="http://cdimage.ubports.com/devices/recovery-vegetahd.img"
)

FILE_CHECKSUMS=(
    ["recovery-vegetahd.img"]="sha256:20a48a1fa62381bde7a9427c902cb43fe46226f11e494ff8743593e81045a23b"
)
