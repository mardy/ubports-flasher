DEVICE_NAME="Oneplus One"
DEVICE_ALIASES="A0001 a0001"

DEVICE_TYPE="android"

BOOTSTRAP_PARTITIONS=(
    ["recovery"]="http://cdimage.ubports.com/devices/recovery-bacon.img"
    ["boot"]="http://cdimage.ubports.com/devices/boot-bacon.img"
    ["LOGO"]="https://github.com/ubports/android_device_oneplus_bacon-1/raw/ubp-5.1/ubuntu_overlay/partitions/LOGO.img"
)

FILE_CHECKSUMS=(
    ["recovery-bacon.img"]="sha256:9b5bf80d1cfc6b21b87d8b29d9a4a02cc9745dd541e3e75180de40be4e92a780"
    ["boot-bacon.img"]="sha256:44c6a5c81a979cd28be3b8e44e78929dbca3c0293a2330c56ee599a69b1c9656"
    ["LOGO.img"]="sha256:57c7e5d940d65452f59a2485ab9182e67d66f6bd6e5bafc0b5f40850eddb1980"
)
