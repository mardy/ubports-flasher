[![pipeline status](https://gitlab.com/mardy/ubports-flasher/badges/master/pipeline.svg)](https://gitlab.com/mardy/ubports-flasher/-/commits/master)
[![coverage report](https://gitlab.com/mardy/ubports-flasher/badges/master/coverage.svg)](https://gitlab.com/mardy/ubports-flasher/-/commits/master)


# UBports flasher

This is a command-line tool to flash the Ubuntu Touch operating system on a
supported device.


## Installation

The program is made available as a [snap](https://snapcraft.io/ubports-flasher)
package. In order to install it, open a terminal and run the following command:

    snap install ubports-flasher

or, if you feel adventurous enough to try out the very latest development version:

    snap install --channel=latest/edge ubports-flasher


### Other installation means

Ubuntu users can add the [UBports tools
PPA](https://launchpad.net/~mardy/+archive/ubuntu/ubports-tools) and install
`ubports-flasher` as a `.deb` package:

```sh
sudo add-apt-repository ppa:mardy/ubports-tools
sudo apt update
sudo apt install ubports-flasher
```

Note that the PPA packages are automatically built from the development branch.

Packages for other distributions are not currently available, but contributions
are very welcome.


### Install from source

If none of the above methods works for you, it's still possible to install
ubports-flasher using its `Makefile`: clone the `git` repository and run

    sudo make install

to install it on the system, or

    make install prefix=~/.local

to install it for your user only (in this case, make sure that the directory
`~/.local/bin` is in your `$PATH`).


## Integrations

UBports flasher has been developed with the goal of making it easy to embed
into a graphical program. There is some more work to be done on this front, but
if you are inclined to develop a graphical frontend for it, please get in touch
(file an issue) and I'll be happy to made the required changes.
