#! /bin/sh

die() {
    echo "$*"
    exit 1
}

test -d po || die "This script must be run from the top-level directory!"

OUTPUT="$1"
shift

for i in "$@"
do
    FILENAME=$(basename $i)
    LANG=${FILENAME%%.po}
    mkdir -p "$OUTPUT/$LANG/LC_MESSAGES/"
    msgfmt -o "$OUTPUT/$LANG/LC_MESSAGES/ubports-flasher.mo" "$i"
done
