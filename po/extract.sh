#! /bin/sh

die() {
    echo "$*"
    exit 1
}

test -d po || die "This script must be run from the top-level directory!"

PACKAGE_NAME="ubports-flasher"
POT_FILE="po/${PACKAGE_NAME}.pot"
xgettext -L Shell \
    --keyword=G \
    --keyword=write_user \
    --keyword=confirm_user \
    -o ${POT_FILE} \
    --package-name=$PACKAGE_NAME \
    flasher \
    device_configs/*.sh \
    device_types/*.sh

# Append the HERE DOC from the help, which is not recognized by xgettext
MAIN_SCRIPT="flasher"
START_LINE=$(grep -n '^Usage:' $MAIN_SCRIPT | cut -d: -f1)

echo >> $POT_FILE
printf "#: %s:%s\n" "$MAIN_SCRIPT" "$START_LINE" >> $POT_FILE
echo 'msgid ""' >> $POT_FILE
grep -A 200 '^Usage:' $MAIN_SCRIPT | while IFS='' read LINE
do
    if [ "$LINE" = "EOF" ]; then
        break
    fi
    printf '"%s\\n"\n' "$LINE" >> $POT_FILE
done
echo 'msgstr ""' >> $POT_FILE
