NAME = ubports-flasher

INSTALL = install

prefix = /usr
BINDIR = $(DESTDIR)/$(prefix)/bin
DATADIR = $(DESTDIR)/$(prefix)/share
LOCALEDIR = $(DATADIR)/locale

PO_FILES := $(wildcard po/*.po)

all: flasher

clean:
	-rm $(patsubst %.po,%.mo,$(PO_FILES))

install: install-bin install-mo install-device-configs install-device-types

install-bin: flasher
	$(INSTALL) -d $(BINDIR)
	$(INSTALL) -m 755 $< $(BINDIR)/ubports-flasher

install-mo: $(patsubst %.po,%.mo,$(PO_FILES))
	for i in $(patsubst po/%.mo,%,$^); do \
		$(INSTALL) -d $(LOCALEDIR)/$$i/LC_MESSAGES; \
		$(INSTALL) -m 644 po/$$i.mo $(LOCALEDIR)/$$i/LC_MESSAGES/$(NAME); \
	done

install-device-configs: $(wildcard device_configs/*.sh)
	$(INSTALL) -d $(DATADIR)/$(NAME)/device_configs
	for i in $^; do \
		$(INSTALL) -m 644 $$i $(DATADIR)/$(NAME)/device_configs/; \
	done

install-device-types: $(wildcard device_types/*.sh)
	$(INSTALL) -d $(DATADIR)/$(NAME)/device_types
	for i in $^; do \
		$(INSTALL) -m 644 $$i $(DATADIR)/$(NAME)/device_types/; \
	done

po/%.mo: po/%.po
	msgfmt -o $@ $<

.PHONY: check
check:
	./tests/bats/bin/bats tests/
