#! /bin/bash

LOADED_DEVICE_TYPES="android $LOADED_DEVICE_TYPES"


# get_device_info
android_get_device_info() {
    local STATE=$(action get_state)
    write_user "Device is currently in '%s' state\n" "$STATE"
    write_user "Product model: %s\n" \
        "$(action device_run getprop "ro.product.model")"
}

# flash_channel
# The channel to be flashed is given in the $CHANNEL variable
android_flash_channel() {
    if [ "$BOOTSTRAP" = "yes" ]; then
        action ensure_boot "bootloader"
        action download_firmware
        action flash_firmware
    elif [ -n "${BOOTSTRAP_PARTITIONS["recovery"]}" ]; then
        # We need to download the recovery partition in any case, in order to
        # reboot to it.
        debug "Downloading recovery partition, to later boot into it"
        mkdir -p "$CACHE_DIR/$DEVICE/firmware"
        download_file "$CACHE_DIR/$DEVICE/firmware" \
            ${BOOTSTRAP_PARTITIONS["recovery"]}
    fi
    action reboot recovery
    action wait_state recovery
    action download_os  # TODO: this could start before, and in the background
    action flash_os
}

# device_run COMMAND...
android_device_run() {
    frun adb shell "$@"
}

# get_state
android_get_state() {
    local ADB_STATE=$(frun adb get-state || true)
    test -n "$ADB_STATE" && echo "$ADB_STATE" && return
    local DEVICE_ID FASTBOOT_STATE
    read DEVICE_ID FASTBOOT_STATE < <(frun fastboot devices)
    debug "Fastboot state is $FASTBOOT_STATE"
    test "$FASTBOOT_STATE" = "fastboot" && echo "bootloader"
}

# wait_state DESIRED_STATE
android_wait_state() {
    local DESIRED_STATE="$1"
    local STATE=$(action get_state)
    if [ "$STATE" != "$DESIRED_STATE" ]; then
        write_user "Waiting for device to be in %s state..." "$DESIRED_STATE"
        if [ "$DESIRED_STATE" = "bootloader" ]; then
            # There's no way to wait for the bootloader; let's just issue the
            # fastboot commands: fastboot will take care of waiting
            write_user "\n"
            return
        fi
        frun adb "wait-for-${DESIRED_STATE}"
        signal_operation_complete
    fi
}

# Returns adb, fastboot (or heimdall and others) depending on how we can reach
# the device
android_get_protocol() {
    local STATE=$(action get_state)
    if [ "$STATE" = "bootloader" ]; then
        echo "fastboot"
    else
        echo "adb"
    fi
}

# reboot [TARGET]
android_reboot() {
    local COMMAND=$(action get_protocol)
    # No quotes around $1 as it could be empty
    local TARGET=$1
    if [ "$COMMAND" == "fastboot" ] && [ "$TARGET" == "recovery" ]; then
        # Special case: the command "fastboot reboot recovery" does not exist.
        # What we can do instead, is boot the downloaded recovery image.
        local PARTITION="recovery"
        local IMAGE_URL="${BOOTSTRAP_PARTITIONS[$PARTITION]}"
        local IMAGE_FILE="$(get_downloaded_file "$IMAGE_URL")"
        if [ -f "$IMAGE_FILE" ]; then
            action boot "$IMAGE_FILE"
        else
            die $ERR_REBOOT "$TARGET"
        fi
    else
        frun "$COMMAND" reboot $TARGET ||
            die $ERR_REBOOT "$TARGET"
    fi
}

# boot IMAGE_FILE
# This must be called when in fastboot mode
android_boot() {
    frun fastboot boot "$IMAGE_FILE"
}

# ensure_boot BOOT_STATE
# Reboot the device into $BOOT_STATE, if it's not in that state already
android_ensure_boot() {
    local DESIRED_STATE="$1"
    local STATE=$(action get_state)
    if [ "$STATE" != "$DESIRED_STATE" ]; then
        action reboot "$DESIRED_STATE"
        action wait_state "$DESIRED_STATE"
    fi
}

# android_adb_format PARTITION
android_adb_format() {
    local PARTITION="$1"
    local BLDEVICE="$(action device_run cat /etc/recovery.fstab |
                      find_partition_device "$PARTITION")"
    if [ -z "$BLDEVICE" ]; then
        debug "Partition $PARTITION not found in fstab; skipping formatting"
        return 1
    fi
    debug "Formatting partition $PARTITION ($BLDEVICE)"
    action device_run umount "/$PARTITION"
    action device_run make_ext4fs "$BLDEVICE"
    action device_run mount "/$PARTITION"
}

# android_bootloader_format PARTITION [TYPE] [SIZE]
android_bootloader_format() {
    # TODO: heimdall
    local PARTITION="$1" TYPE="$2" SIZE="$3"
    local OPTIONS=""
    test -n "$TYPE" && OPTIONS="${OPTIONS}:$TYPE"
    test -n "$SIZE" && OPTIONS="${OPTIONS}:$SIZE"
    frun fastboot format${OPTIONS} "$PARTITION"
}

# format PARTITION
android_format() {
    if [ $(action get_state) == "bootloader" ]; then
        android_bootloader_format "$@"
    else
        android_adb_format "$@"
    fi
}

# device_wipe_cache
android_device_wipe_cache() {
    action format "cache" || action device_run rm -rf "/cache/*"
}

# device_prepare_dirs
# Make sure that the target device is ready to receive the OS image files
android_device_prepare_dirs() {
    write_user "Mounting partitions..."
    action device_run mount -a || true  # We allow this to fail
    write_user "Cleaning cache..."
    action device_wipe_cache
    action device_run mkdir -p /cache/recovery
}

# prepare_partition_images
# This step can be used to unpack zip files, or perform changes on the
# downloaded files.
# This function must set the BOOTSTRAP_PARTITION_IMAGES variable.
android_prepare_partition_images() {
    for PARTITION in "${!BOOTSTRAP_PARTITIONS[@]}"; do
        local IMAGE_URL="${BOOTSTRAP_PARTITIONS[$PARTITION]}"
        local IMAGE_FILE="$(get_downloaded_file "$IMAGE_URL")"
        BOOTSTRAP_PARTITION_IMAGES[$PARTITION]="$IMAGE_FILE"
    done
}

# flash_firmware
android_flash_firmware() {
    action prepare_partition_images
    android_wait_state bootloader
    for PARTITION in "${!BOOTSTRAP_PARTITION_IMAGES[@]}"
    do
        local IMAGE_FILE="${BOOTSTRAP_PARTITION_IMAGES[$PARTITION]}"
        # TODO: consider redirecting the output
        fastboot flash "$PARTITION" "$IMAGE_FILE"
    done
}

# push_files DEST_DIR FILE [FILE...]
android_push_files() {
    local DEST_DIR="$1"
    shift
    adb push "$@" "$DEST_DIR/"
}
