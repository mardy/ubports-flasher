setup() {
	load 'test_helper/bats-support/load'
	load 'test_helper/bats-assert/load'

	FLASHER="./flasher"
	GITHUB="https://github.com/ubuntu-touch-begonia/ubuntu-touch-begonia/releases/download"
	FW_URL="${GITHUB}/20210810"
	OS_URL="${GITHUB}/ota-19"
	WGET="wget .*"
	MOCKED_ADB_STATE=bootloader
}

function adb() {
	case "$1" in
		get-state) echo $MOCKED_ADB_STATE;;
		*) echo "adb $@";;
	esac
}

function fastboot() {
	echo "fastboot $@"
}

function unzip() {
	echo "unzip $@"
}

function wget() {
	echo "wget $@"
}

function sha256sum() {
	local FN="$(basename "$1")"
	local CHECKSUM=$(echo "${FILE_CHECKSUMS["$FN"]}" | cut -d':' -f2)
	echo "$CHECKSUM $FN"
}

@test "begonia download no bootstrap" {
	source "$FLASHER"
	SCRIPT_DATA_DIR="."
	run main -d begonia -q download
	local FW="-P .*/\\.cache/ubports/begonia/firmware"
	local OS="-P .*/\\.cache/ubports/begonia/Ubuntu Touch"
	assert_line -e "$WGET $OS $OS_URL/boot\\.img"
	assert_line -e "$WGET $OS $OS_URL/system\\.zip"
	# Check that a few files from the firmware are *not* downloaded
	refute_line -e "$WGET $FW $FW_URL/recovery\\.img"
	refute_line -e "$WGET $FW $FW_URL/dtbo\\.img"
	refute_line -e "$WGET $FW $FW_URL/vendor\\.zip"
}

@test "begonia download" {
	source "$FLASHER"
	SCRIPT_DATA_DIR="."
	run main -d begonia -q -b download
	local FW="-P .*/\\.cache/ubports/begonia/firmware"
	local OS="-P .*/\\.cache/ubports/begonia/Ubuntu Touch"
	assert_line -e "$WGET $FW $FW_URL/vbmeta\\.img"
	assert_line -e "$WGET $FW $FW_URL/recovery\\.img"
	assert_line -e "$WGET $FW $FW_URL/dtbo\\.img"
	assert_line -e "$WGET $FW $FW_URL/vendor\\.zip"
	assert_line -e "$WGET $FW $FW_URL/fw_begonia_.*\\.zip"
	assert_line -e "$WGET $OS $OS_URL/boot\\.img"
	assert_line -e "$WGET $OS $OS_URL/system\\.zip"
}

@test "begonia flash" {
	source "$FLASHER"
	SCRIPT_DATA_DIR="."
	run main -d begonia -q -b flash
	local FW="-P .*/\\.cache/ubports/begonia/firmware"
	local OS="-P .*/\\.cache/ubports/begonia/Ubuntu Touch"
	assert_line -e "$WGET $FW $FW_URL/vbmeta\\.img"
	assert_line -e "$WGET $FW $FW_URL/recovery\\.img"
	assert_line -e "$WGET $FW $FW_URL/dtbo\\.img"
	assert_line -e "$WGET $FW $FW_URL/vendor\\.zip"
	assert_line -e "$WGET $FW $FW_URL/fw_begonia_.*\\.zip"
	assert_line -e "unzip .*/\\.cache/ubports/begonia/firmware/vendor.zip -d /tmp/.*"
	assert_line -e "unzip .*/\\.cache/ubports/begonia/firmware/fw_begonia.*.zip -d /tmp/.*"
	assert_output -e - <<-EOF
	fastboot flash md1img .*/md1img.img
	fastboot flash vbmeta .*/.cache/ubports/begonia/firmware/vbmeta.img
	fastboot flash preloader .*/preloader_ufs.img
	fastboot flash spmfw .*/spmfw.img
	fastboot flash recovery .*/.cache/ubports/begonia/firmware/recovery.img
	fastboot flash dtbo .*/.cache/ubports/begonia/firmware/dtbo.img
	fastboot flash cam_vpu1 .*/cam_vpu1.img
	fastboot flash cam_vpu2 .*/cam_vpu2.img
	fastboot flash cam_vpu3 .*/cam_vpu3.img
	fastboot flash vendor .*/vendor.img
	fastboot flash sspm_2 .*/sspm.img
	fastboot flash sspm_1 .*/sspm.img
	fastboot flash audio_dsp .*/audio_dsp.img
	fastboot flash lk .*/lk.img
	fastboot flash gz2 .*/gz.img
	fastboot flash gz1 .*/gz.img
	fastboot flash tee2 .*/tee.img
	fastboot flash tee1 .*/tee.img
	fastboot flash lk2 .*/lk.img
	fastboot flash scp2 .*/scp.img
	fastboot flash scp1 .*/scp.img
	EOF
	assert_line -e "$WGET $OS $OS_URL/boot\\.img"
	assert_line -e "$WGET $OS $OS_URL/system\\.zip"
	assert_line -e "unzip .*/\\.cache/ubports/begonia/Ubuntu Touch/system.zip -d /tmp/.*"
	assert_output -e - <<-EOF
	fastboot flash system .*/system.img
	fastboot flash boot .*/.cache/ubports/begonia/Ubuntu Touch/boot.img
	fastboot reboot
	EOF
	refute_line -p "fastboot format"
}
@test "begonia flash with wipe" {
	source "$FLASHER"
	SCRIPT_DATA_DIR="."
	run main -d begonia -q --wipe flash
	local FW="-P .*/\\.cache/ubports/begonia/firmware"
	local OS="-P .*/\\.cache/ubports/begonia/Ubuntu Touch"
	# We don't check the all the commands, just a few samples
	assert_line -e "$WGET $OS $OS_URL/boot\\.img"
	assert_line -e "$WGET $OS $OS_URL/system\\.zip"
	assert_line -e "unzip .*/\\.cache/ubports/begonia/Ubuntu Touch/system.zip -d /tmp/.*"
	assert_output -e - <<-EOF
	fastboot flash system .*/system.img
	fastboot flash boot .*/.cache/ubports/begonia/Ubuntu Touch/boot.img
	fastboot reboot
	EOF
	assert_line "fastboot format:ext4 userdata"
}
