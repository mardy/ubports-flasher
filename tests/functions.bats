# vim: noet

setup() {
	load 'test_helper/bats-support/load'
	load 'test_helper/bats-assert/load'

	FLASHER="./flasher"
	source "$FLASHER"
	VERBOSITY_LEVEL=0
	LOG_FILE=$(mktemp -t test_commands.log.XXXXXX)
	TMP_DIR=$(mktemp -d --tmpdir test-tmp.XXXXXX)
}

teardown() {
	rm -f $LOG_FILE
}

@test "debug" {
	VERBOSITY_LEVEL=2 debug ciao 1> stdout 2> stderr
	grep ciao stderr
	test "$(cat stdout)" = ""

	VERBOSITY_LEVEL=1 debug ciao 1> stdout 2> stderr
	test "$(cat stderr)" = ""
	test "$(cat stdout)" = ""

	rm stdout stderr
}

@test "confirm_user" {
	function write_user() {
		echo "Written $@"
	}
	VERBOSITY_LEVEL=1 run confirm_user one two three < <(yes)
	assert_output "Written one\n two three"

	VERBOSITY_LEVEL=0 run confirm_user one two three
	assert_output ""
}

print_stdout_and_stderr() {
	echo "$1"
	>&2 echo "$2"
}

@test "frun" {
	function debug() {
		test $VERBOSITY_LEVEL -ge 2 && echo debug "$@"
	}
	TMP_DIR="/tmp"

	VERBOSITY_LEVEL=2 run frun print_stdout_and_stderr "some thing" "some error"
	assert_line -p "debug Running print_stdout_and_stderr"
	assert_line "some thing"
	assert_line "debug some error"

	VERBOSITY_LEVEL=1 run frun print_stdout_and_stderr "some thing" "some error"
	assert_output "some thing"
}

@test "frun --or-die" {
	function die() {
		echo dead "$@"
	}

	TMP_DIR="/tmp"
	run frun --or-die false
	assert_output -e "^dead $ERR_COMMAND false *$"
}

@test "download_files" {
	function frun() {
		echo run "$@"
	}

	function wrapper() {
		declare -A DOWNLOADED_FILES
		download_files "$@"
		for i in "${!DOWNLOADED_FILES[@]}"
		do
			echo "${i}=${DOWNLOADED_FILES[$i]}"
		done
	}
	run wrapper output_dir "https://one" "https://two"
	assert_line "run --or-die wget --no-config -r -nd -N -nv --content-disposition -P output_dir https://one"
	assert_line "run --or-die wget --no-config -r -nd -N -nv --content-disposition -P output_dir https://two"
	assert_line "https://one=output_dir/one"
	assert_line "https://two=output_dir/two"
}

@test "find_partition_device" {
	run find_partition_device home <<-'EOF'
	/dev/disk/by-label/Swap none swap sw 0 0
	/dev/disk/by-label/Home /home auto nosuid,nodev,nofail 0 0
	/dev/sda2 /usr auto nodev 0 0
	EOF
	assert_output "/dev/disk/by-label/Home"
}

@test "detect_device" {
	local ADB_ANSWER=
	local FASTBOOT_ANSWER=
	declare -g DEVICE=
	adb_detect_device() {
		echo adb_detect_device "$@" >> $LOG_FILE
		echo $ADB_ANSWER
	}
	fastboot_detect_device() {
		echo fastboot_detect_device "$@" >> $LOG_FILE
		echo $FASTBOOT_ANSWER
	}

	detect_device_wrapper() {
		detect_device "$@" && echo "Detected device $DEVICE"
	}

	# Neither backend succeeds
	run detect_device_wrapper
	test "$status" -eq 1
	test -z "$DEVICE"
	test -z "$DEVICE_TYPE"
	run cat $LOG_FILE
	assert_line "adb_detect_device"
	assert_line "fastboot_detect_device"

	# adb backend succeeds
	ADB_ANSWER="fremantle"
	run detect_device_wrapper
	test "$status" -eq 0
	assert_line "Detected device fremantle"

	# fastboot backend succeeds
	ADB_ANSWER=
	FASTBOOT_ANSWER="gagarin"
	run detect_device_wrapper
	test "$status" -eq 0
	assert_line "Detected device gagarin"
}

@test "adb_detect_device" {
	local ADB_ANSWER=
	local ADB_STDERR=
	adb() {
		echo adb "$@" >> $LOG_FILE
		echo $ADB_ANSWER
		>&2 echo $ADB_STDERR
	}

	# unauthorized
	ADB_STDERR="device unauthorized"
	run adb_detect_device
	test "$status" -eq 1
	assert_line "device unauthorized"

	# found
	ADB_STDERR=
	ADB_ANSWER="  elephanta  " # to test stripping of whitespaces
	run adb_detect_device
	test "$status" -eq 0
	assert_line "elephanta"

	# not found
	rm $LOG_FILE
	ADB_ANSWER=
	run adb_detect_device
	test "$status" -eq 0
	refute_output -e ".+"
	run cat $LOG_FILE
	assert_line "adb shell getprop ro.product.vendor.device"
	assert_line "adb shell getprop ro.product.device"
}

@test "fastboot_detect_device" {
	local FASTBOOT_ANSWER_devices=
	local FASTBOOT_ANSWER_getvar=
	fastboot() {
		echo fastboot "$@" >> $LOG_FILE
		local VAR="FASTBOOT_ANSWER_$1"
		echo -ne "${!VAR}"
	}

	# no devices
	run fastboot_detect_device
	test "$status" -eq 0
	refute_output -e ".+"
	run cat $LOG_FILE
	#assert_output "fastboot devices"

	# found
	rm $LOG_FILE
	FASTBOOT_ANSWER_devices="12345678\n"
	FASTBOOT_ANSWER_getvar="product: brick \n"
	run fastboot_detect_device
	test "$status" -eq 0
	assert_output "brick"
	run cat $LOG_FILE
	assert_output - <<-EOF
	fastboot devices
	fastboot getvar product
	EOF
}
