setup() {
    load 'test_helper/bats-support/load'
    load 'test_helper/bats-assert/load'

    FLASHER="./flasher"
}

@test "help" {
    run "$FLASHER" -h
    assert_output -p "Usage: flasher [options] [command]"
    run "$FLASHER" --help
    assert_output -p "Usage: flasher [options] [command]"
}

@test "invalid action" {
    run ! "$FLASHER" non-existing
    assert_output -p "Invalid command-line option: non-existing"
}
