setup() {
	load 'test_helper/bats-support/load'
	load 'test_helper/bats-assert/load'

	FLASHER="./flasher"
	WGET="wget .*"
	MOCKED_ADB_STATE=bootloader
	HOME="$(mktemp -d --tmpdir test-home.XXXXXX)"
	mkdir -p "$HOME/.cache/ubports/krillin/"
	COMMANDS="$HOME/test-commands.log"
}

function adb() {
	case "$1" in
		get-state) echo $MOCKED_ADB_STATE;;
	esac
	echo "adb $@" >> $COMMANDS
}

function fastboot() {
	echo "fastboot $@" >> $COMMANDS
}

function unzip() {
	echo "unzip $@" >> $COMMANDS
}

function wget() {
	local INDEX_JSON="$TMP_DIR/index.json"
	cat > "$INDEX_JSON" <<-END
	{
		"images": [
			{
				"description": "ubports=20220209-986,device=20210430-1959,keyring=archive-master,tag=OTA-22,version=20",
				"files": [
					{
						"checksum": "3f3b",
						"order": 0,
						"path": "/pool/ubports-5787.tar.xz",
						"signature": "/pool/ubports-5787.tar.xz.asc",
						"size": 369132452
					},
					{
						"checksum": "0d24",
						"order": 1,
						"path": "/pool/device-aebd.tar.xz",
						"signature": "/pool/device-aebd.tar.xz.asc",
						"size": 72582812
					},
					{
						"checksum": "aa88",
						"order": 2,
						"path": "/pool/keyring-2899.tar.xz",
						"signature": "/pool/keyring-2899.tar.xz.asc",
						"size": 1564
					},
					{
						"checksum": "be003a5750e0b793a54c3bbd52385ab13aaae0c6c1eea3a60505daac2762dc3b",
						"order": 3,
						"path": "/ubports-touch/16.04/stable/krillin/version-20.tar.xz",
						"signature": "/ubports-touch/16.04/stable/krillin/version-20.tar.xz.asc",
						"size": 468
					}
				],
				"type": "full",
				"version": 20,
				"version_detail": "ubports=20220209-986,device=20210430-1959,keyring=archive-master,tag=OTA-22,version=20"
			}
		]
	}
	END

	echo "wget $@" >> $COMMANDS
}

function sha256sum() {
	local FN="$(basename "$1")"
	local CHECKSUM=$(echo "${FILE_CHECKSUMS["$FN"]}" | cut -d':' -f2)
	echo "$CHECKSUM $FN"
}

@test "krillin download no bootstrap" {
	source "$FLASHER"
	SCRIPT_DATA_DIR="."
	# Create the channels.json file
	local CHANNELS_JSON="$HOME/.cache/ubports/krillin/channels.json"
	cat > "$CHANNELS_JSON" <<-END
	{
		"ubports-touch/16.04/stable": {
			"devices": {
				"krillin": {
					"index": "/ubports-touch/16.04/stable/krillin/index.json"
				}
			}
		}
	}
	END
	run main -d krillin -q download
	run cat $COMMANDS
	local SI="https://system-image.ubports.com"
	assert_output -e - <<-END
	$WGET $SI/channels.json
	$WGET $SI//?ubports-touch/16.04/stable/krillin/index.json
	$WGET $SI/gpg/image-signing.tar.xz
	$WGET $SI/gpg/image-signing.tar.xz.asc
	$WGET $SI/gpg/image-master.tar.xz
	$WGET $SI/gpg/image-master.tar.xz.asc
	$WGET $SI//?pool/ubports-5787.tar.xz
	$WGET $SI//?pool/ubports-5787.tar.xz.asc
	$WGET $SI//?pool/device-aebd.tar.xz
	$WGET $SI//?pool/device-aebd.tar.xz.asc
	$WGET $SI//?pool/keyring-2899.tar.xz
	$WGET $SI//?pool/keyring-2899.tar.xz.asc
	$WGET $SI//?ubports-touch/16.04/stable/krillin/version-20.tar.xz
	$WGET $SI//?ubports-touch/16.04/stable/krillin/version-20.tar.xz.asc
	END
}

@test "krillin download bootstrap" {
	source "$FLASHER"
	SCRIPT_DATA_DIR="."
	# Create the channels.json file
	local CHANNELS_JSON="$HOME/.cache/ubports/krillin/channels.json"
	cat > "$CHANNELS_JSON" <<-END
	{
		"ubports-touch/16.04/stable": {
			"devices": {
				"krillin": {
					"index": "/ubports-touch/16.04/stable/krillin/index.json"
				}
			}
		}
	}
	END
	run main -d krillin -b -q download
	run cat $COMMANDS
	local SI="https://system-image.ubports.com"
	assert_output -e - <<-END
	$WGET $SI/channels.json
	$WGET $SI//?ubports-touch/16.04/stable/krillin/index.json
	$WGET $HOME/.cache/ubports/krillin/firmware http://cdimage.ubports.com/devices/recovery-krillin.img
	$WGET $SI/gpg/image-signing.tar.xz
	$WGET $SI/gpg/image-signing.tar.xz.asc
	$WGET $SI/gpg/image-master.tar.xz
	$WGET $SI/gpg/image-master.tar.xz.asc
	$WGET $SI//?pool/ubports-5787.tar.xz
	$WGET $SI//?pool/ubports-5787.tar.xz.asc
	$WGET $SI//?pool/device-aebd.tar.xz
	$WGET $SI//?pool/device-aebd.tar.xz.asc
	$WGET $SI//?pool/keyring-2899.tar.xz
	$WGET $SI//?pool/keyring-2899.tar.xz.asc
	$WGET $SI//?ubports-touch/16.04/stable/krillin/version-20.tar.xz
	$WGET $SI//?ubports-touch/16.04/stable/krillin/version-20.tar.xz.asc
	END
}

@test "krillin flash bootstrap" {
	source "$FLASHER"
	SCRIPT_DATA_DIR="."
	# Create the channels.json file
	local CHANNELS_JSON="$HOME/.cache/ubports/krillin/channels.json"
	cat > "$CHANNELS_JSON" <<-END
	{
		"ubports-touch/16.04/devel": {
			"devices": {
				"krillin": {
					"index": "/ubports-touch/16.04/stable/krillin/index.json"
				}
			}
		}
	}
	END
	mkdir -p "$CACHE_DIR/krillin/firmware/"
	touch "$CACHE_DIR/krillin/firmware/recovery-krillin.img"
	run main -d krillin -c devel -b -q flash
	run cat $COMMANDS
	local SI="https://system-image.ubports.com"
	assert_output -e - <<-END
	$WGET $SI/channels.json
	$WGET $SI//?ubports-touch/16.04/stable/krillin/index.json
	.*
	$WGET $HOME/.cache/ubports/krillin/firmware http://cdimage.ubports.com/devices/recovery-krillin.img
	adb get-state
	fastboot flash recovery $HOME/.cache/ubports/krillin/firmware/recovery-krillin.img
	.*
	adb get-state
	adb wait-for-recovery
	.*
	$WGET $SI//?pool/ubports-5787.tar.xz
	$WGET $SI//?pool/ubports-5787.tar.xz.asc
	.*
	adb push .* $HOME/.cache/ubports/krillin/Ubuntu Touch/ubports-5787.tar.xz .*
	adb get-state
	fastboot boot .*recovery-krillin.img
	END
}
